const esbuild = require("esbuild");

esbuild.buildSync({
  entryPoints: ["src/index.tsx"],
  outfile: "dist/bundle.js",
  bundle: true,
  treeShaking: true,
  minify: true,
  sourcemap: true,
  platform: "browser",
});
