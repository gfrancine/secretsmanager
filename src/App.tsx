import React, { useState } from "react";
import { Data } from "./types";
import { Editor } from "./Editor";
import { FileMenu } from "./FileMenu";
import { parseFile, serializeFile } from "./sourcefile";

const defaultData = {
  password: "",
  entries: [],
};

function download(file: string, fileName: string) {
  const a = document.createElement("a");
  a.href = URL.createObjectURL(new Blob([file]));
  a.setAttribute("download", fileName);
  a.click();
}

export function App() {
  const [data, setData] = useState<Data>({ ...defaultData });

  return (
    <div>
      <div>
        <FileMenu
          handleImport={async (contents, password) => {
            const data = await parseFile(contents, password);
            setData(data);
          }}
          handleExport={async () => {
            const string = await serializeFile(data);
            download(string, "passwords.json");
          }}
        />
      </div>
      <br />
      <div>
        <Editor data={data} setData={setData} />
      </div>
    </div>
  );
}