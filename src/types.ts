export type FieldData = {
  id: string;
  name: string;
  value: string;
  show: boolean;
};

export type EntryData = {
  id: string;
  name: string;
  fields: FieldData[];
};

export type Data = {
  password: string;
  entries: EntryData[];
};
