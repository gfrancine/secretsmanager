import React, { Fragment, useState } from "react";
import { FieldData, EntryData, Data } from "./types";
import { nanoid } from "nanoid";

function Field(
  props: FieldData & {
    setFieldData?: (value: FieldData) => unknown;
    deleteField?: () => unknown;
  }
) {
  const { name, value, show, id } = props;
  const [data, setData] = useState<FieldData>({ id, name, value, show });

  const setFieldData = (data: FieldData) => {
    setData(data);
    props.setFieldData?.(data);
  };

  return (
    <tr>
      <td>
        <input
          type="text"
          placeholder="field"
          value={data.name}
          onChange={(e) =>
            setFieldData({
              ...data,
              name: e.target.value,
            })
          }
        />
      </td>
      <td>
        <input
          type={data.show ? "text" : "password"}
          placeholder="value"
          value={data.value}
          onChange={(e) =>
            setFieldData({
              ...data,
              value: e.target.value,
            })
          }
        />
        <button
          onClick={() =>
            setFieldData({
              ...data,
              show: !data.show,
            })
          }
        >
          {data.show ? "hide" : "show"}
        </button>
      </td>
      <td>
        <button onClick={() => navigator.clipboard.writeText(data.value)}>
          copy
        </button>
        <button onClick={props.deleteField}>delete</button>
      </td>
    </tr>
  );
}

function Entry(
  props: EntryData & {
    setEntryData?: (data: EntryData) => unknown;
    deleteEntry?: () => unknown;
  }
) {
  const { fields, name, id } = props;

  const deleteField = (index: number) => {
    const newFields = [...fields];
    newFields.splice(index, 1);
    props.setEntryData?.({ id, name, fields: newFields });
  };

  const setField = (index: number, data: FieldData) => {
    const newFields = [...fields];
    newFields[index] = data;
    props.setEntryData?.({ id, name, fields: newFields });
  };

  const addNewField = () =>
    props.setEntryData?.({
      id,
      name,
      fields: [
        ...fields,
        {
          id: nanoid(),
          show: true,
          name: "",
          value: "",
        },
      ],
    });

  return (
    <div>
      <div>
        <label>entry name</label>
        <input
          type="text"
          placeholder="entry name"
          value={props.name}
          onChange={(e) =>
            props.setEntryData?.({
              id,
              fields,
              name: e.target.value,
            })
          }
        />
        <button onClick={props.deleteEntry}>delete</button>
      </div>
      <div>
        <table>
          <tbody>
            {props.fields.map((field, i) => (
              <Field
                key={field.id}
                id={field.id}
                name={field.name}
                show={field.show}
                value={field.value}
                setFieldData={(value) => setField(i, value)}
                deleteField={() => deleteField(i)}
              />
            ))}
            <tr>
              <td colSpan={3}>
                <button onClick={addNewField}>add new field</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

function SearchBar(props: {
  placeholder?: string;
  handleSearch?: (term: string) => unknown;
  handleClear?: () => unknown;
}) {
  const [searchTerm, setSearchTerm] = useState("");
  const [isSearching, setIsSearching] = useState(false);

  const search = () => {
    setIsSearching(true);
    props.handleSearch?.(searchTerm);
  };

  const clear = () => {
    setSearchTerm("");
    setIsSearching(false);
    props.handleClear?.();
  };

  return (
    <div>
      <label>search entries</label>
      <input
        value={searchTerm}
        placeholder={props.placeholder}
        onChange={(e) => setSearchTerm(e.target.value)}
        onKeyUp={(e) => {
          if (e.key === "Enter") search();
        }}
      />
      <button onClick={search}>search</button>
      {isSearching ? <button onClick={clear}>clear</button> : <Fragment />}
    </div>
  );
}

export function Editor(props: {
  data: Data;
  setData: (data: Data) => unknown;
}) {
  const { data, setData } = props;

  const setEntry = (index: number, entry: EntryData) => {
    const newEntries = [...data.entries];
    newEntries[index] = entry;
    setData({ ...data, entries: newEntries });
  };

  const deleteEntry = (index: number) => {
    const newEntries = [...data.entries];
    newEntries.splice(index, 1);
    setData({ ...data, entries: newEntries });
  };

  const addNewEntry = () =>
    setData({
      ...data,
      entries: [
        {
          id: nanoid(),
          name: "",
          fields: [],
        },
        ...data.entries,
      ],
    });

  const [showPassword, setShowPassword] = useState(false);

  const [searchTerm, setSearchTerm] = useState("");

  return (
    <div>
      <div>
        <label>master password</label>
        <input
          placeholder="password"
          type={showPassword ? "text" : "password"}
          value={data.password}
          onChange={(e) => setData({ ...data, password: e.target.value })}
        />
        <button onClick={() => setShowPassword(!showPassword)}>
          {showPassword ? "hide" : "show"}
        </button>
      </div>
      <div>
        <SearchBar
          placeholder="search entries"
          handleClear={() => setSearchTerm("")}
          handleSearch={setSearchTerm}
        />
        <button onClick={addNewEntry}>add new entry</button>
      </div>
      <div>
        {data.entries.map((entry, i) =>
          searchTerm.length === 0 ||
          entry.name.toLowerCase().match(searchTerm.toLowerCase()) ? (
            <Fragment key={entry.id}>
              <hr />
              <Entry
                id={entry.id}
                name={entry.name}
                fields={entry.fields}
                setEntryData={(value) => setEntry(i, value)}
                deleteEntry={() => deleteEntry(i)}
              />
            </Fragment>
          ) : (
            <Fragment key={entry.id} />
          )
        )}
      </div>
    </div>
  );
}
