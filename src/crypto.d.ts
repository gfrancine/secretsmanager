declare function aesGcmEncrypt(
  plaintext: string,
  password: string
): Promise<string>;

declare function aesGcmDecrypt(
  encrypted: string,
  password: string
): Promise<string>;

export { aesGcmEncrypt, aesGcmDecrypt };
