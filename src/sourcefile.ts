import { aesGcmDecrypt, aesGcmEncrypt } from "./crypto";
import { Data } from "./types";

type FileData = {
  version: 1;
  data: string;
};

export async function parseFile(json: string, password: string) {
  const parsed = JSON.parse(json);

  if (
    typeof parsed !== "object" ||
    typeof parsed.version !== "number" ||
    typeof parsed.data !== "string"
  ) {
    throw new Error("Invalid file");
  }

  const fileData = parsed as FileData;
  const jsonData = await aesGcmDecrypt(fileData.data, password);
  const data: Data = JSON.parse(jsonData);

  return data;
}

export async function serializeFile(data: Data) {
  return JSON.stringify({
    version: 1,
    data: await aesGcmEncrypt(JSON.stringify(data), data.password),
  });
}
