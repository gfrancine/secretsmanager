import React, { useState } from "react";

function FileInput(props: { file?: File; onChange?: (file: File) => unknown }) {
  const setFile = (file: File | undefined | null) => {
    if (!file) return;
    props.onChange?.(file);
  };

  return (
    <div
      onDragOver={(e) => e.preventDefault()}
      onDrop={(e) => setFile(e.dataTransfer.files.item(0))}
    >
      <label>drop or choose file</label>
      <input type="file" onChange={(e) => setFile(e.target.files?.item(0))} />
    </div>
  );
}

export function FileMenu(props: {
  handleImport?: (file: string, password: string) => unknown;
  handleExport?: () => unknown;
}) {
  const [password, setPassword] = useState("");
  const [file, setFile] = useState<File | undefined>();
  const [error, setError] = useState("");

  const setErrorFromValue = (e: unknown) => {
    const message = e instanceof Error ? e.message : "" + e;
    setError(message);
  };

  return (
    <div>
      <div>
        <div>{error}</div>
        <div>
          <FileInput file={file} onChange={setFile} />
        </div>
        <div>
          <label>password</label>
          <input
            type="password"
            placeholder="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div>
          <button
            onClick={async () => {
              if (!file) {
                setError("no file selected");
                return;
              }

              let json = "";
              try {
                json = await file.text();
              } catch (e) {
                setErrorFromValue(e);
                return;
              }

              try {
                await props.handleImport?.(json, password);
              } catch (e) {
                setErrorFromValue(e);
                return;
              }

              setError("");
            }}
          >
            import
          </button>
        </div>
      </div>
      <br />
      <div>
        <button onClick={props.handleExport}>export to file</button>
      </div>
    </div>
  );
}
